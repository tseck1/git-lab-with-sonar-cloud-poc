package org.hsmith.medium;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WorkerClassTest {
    @Test
    void sum() {
        WorkerClass worker = new WorkerClass();
        assertEquals(2, worker.sum(1, 1));
    }

    @Test
    void determineOrderCorrectTest() {
        WorkerClass worker = new WorkerClass();
        assertEquals("a > b > c", worker.determineOrderForThreeInteger(8, 5, 1));
    }

    @Test
    void determineOrderIncorrectTest() {
        WorkerClass worker = new WorkerClass();
        assertNotEquals("a > b > c", worker.determineOrderForThreeInteger(1, 5, 3));
    }
}