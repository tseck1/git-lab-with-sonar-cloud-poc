package org.hsmith.medium;

final class WorkerClass {
    int sum(final int value1, final int value2) {
        return value1 + value2;
    }

    String determineOrderForThreeInteger(final int a, final int b, final int c) {
        String result=null;

        if (a > b) {
            if (b > c) {
                result = "a > b > c";
            } else {
                if (c > a) {
                    result = "c > a > b";
                } else {
                    result = "a > c > b";
                }
            }
        } else { // b > a
            if (a > c) {
                result = "b > a > c";
            } else {
                if (c > b) {
                    result = "c > b > a";
                } else {
                    result = "b > c > a";
                }
            }
        }

        if (a > b) {
            if (b > c) {
                result = "a > b > c";
            } else {
                if (c > a) {
                    result = "c > a > b";
                } else {
                    result = "a > c > b";
                }
            }
        } else { // b > a
            if (a > c) {
                result = "b > a > c";
            } else {
                if (c > b) {
                    result = "c > b > a";
                } else {
                    result = "b > c > a";
                }
            }
        }

        return result;
    }
}
